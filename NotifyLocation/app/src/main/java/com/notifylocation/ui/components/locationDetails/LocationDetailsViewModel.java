package com.notifylocation.ui.components.locationDetails;

import com.notifylocation.BR;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.notifylocation.R;
import com.notifylocation.events.OnUserLocationsChanged;
import com.notifylocation.models.UserLocation;

import org.greenrobot.eventbus.EventBus;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.SeekBar;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Maryan Melnychuk on 9/11/16.
 */

public class LocationDetailsViewModel extends BaseObservable implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks {

    private boolean mAdding;
    private UserLocation mLocation;

    private GoogleApiClient mGoogleApiClient;
    private LocationDetailsActivity mActivity;

    private GoogleMap mMap;
    private Location mCurrentLocation;
    private Marker mCurrentMarker;

    public LocationDetailsViewModel(LocationDetailsActivity activity, UserLocation userLocation) {
        mActivity = activity;
        mAdding = userLocation == null;
        if(userLocation == null) {
            mLocation = new UserLocation();
            mLocation.setEnabled(true);
            mLocation.setRadius(200);
        } else {
            mLocation = userLocation;
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        if(!mAdding){
            mCurrentMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(mLocation.getLatitude(), mLocation.getLongitude())));
            moveMapToLocation(mLocation.getLatitude(), mLocation.getLongitude());
        }
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(mCurrentMarker != null) {
                    mCurrentMarker.remove();
                }
                mCurrentMarker = mMap.addMarker(new MarkerOptions().position(latLng));
            }
        });
    }

    public void onNameEntered(Editable text) {
        mLocation.setName(text.toString());
    }

    public void onRadiusChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (progress) {
            case 0:
                mLocation.setRadius(50);
                break;
            case 1:
                mLocation.setRadius(100);
                break;
            case 2:
                mLocation.setRadius(200);
                break;
            case 3:
                mLocation.setRadius(500);
                break;
            case 4:
                mLocation.setRadius(1000);
                break;
            case 5:
                mLocation.setRadius(2000);
                break;
            case 6:
                mLocation.setRadius(5000);
                break;
        }
        notifyPropertyChanged(BR.radius);
    }

    public void onSave(View view) {
        if(mCurrentMarker == null){
            mActivity.showSnackbar(R.string.error_no_location);
        } else if(TextUtils.isEmpty(mLocation.getName())){
            mActivity.showSnackbar(R.string.error_no_name);
        } else {
            if(mAdding){
                mLocation.setGuid(UUID.randomUUID().toString());
            }
            mLocation.setLatitude(mCurrentMarker.getPosition().latitude);
            mLocation.setLongitude(mCurrentMarker.getPosition().longitude);
            Realm db = Realm.getDefaultInstance();
            db.executeTransaction(new Realm.Transaction(){
                @Override
                public void execute(Realm realm) {
                    if(mAdding) {
                        realm.copyToRealm(mLocation);
                    } else {
                        realm.copyToRealmOrUpdate(mLocation);
                    }
                    mActivity.finish();
                    EventBus.getDefault().post(new OnUserLocationsChanged());
                }
            });
        }
    }

    public void onDelete(View view) {
        Realm db = Realm.getDefaultInstance();
        db.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<UserLocation> result = realm.where(UserLocation.class).equalTo(UserLocation.GUID, mLocation.getGuid()).findAll();
                result.deleteAllFromRealm();
                mActivity.finish();
                EventBus.getDefault().post(new OnUserLocationsChanged());
            }
        });
    }

    public int getRadiusPosition() {
        switch (mLocation.getRadius()) {
            case 50:
                return 0;
            case 100:
                return 1;
            case 200:
                return 2;
            case 500:
                return 3;
            case 1000:
                return 4;
            case 2000:
                return 5;
            case 5000:
                return 6;
            default:
                return 0;
        }
    }

    @Bindable
    public String getRadius() {
        if (mLocation.getRadius() < 1000) {
            return " " + String.valueOf(mLocation.getRadius()) + "m";
        } else {
            return " " + String.valueOf(mLocation.getRadius()).substring(0, 1) + "km";
        }
    }

    public boolean isAdding() {
        return mAdding;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(mAdding) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mMap != null && mCurrentLocation != null) {
                moveMapToLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            }
        }
    }

    private void moveMapToLocation(double lat, double lon) {
        LatLng user = new LatLng(lat, lon);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(user, 17), 2000, null);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        mGoogleApiClient = googleApiClient;
    }

    public UserLocation getLocation() {
        return mLocation;
    }
}
