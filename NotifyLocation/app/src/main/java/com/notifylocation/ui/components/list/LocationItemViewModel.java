package com.notifylocation.ui.components.list;

import com.notifylocation.models.UserLocation;

import io.realm.Realm;

/**
 * Created by Maryan Melnychuk on 9/11/16.
 */

public class LocationItemViewModel {
    private UserLocation mUserLocation;

    public LocationItemViewModel(UserLocation userLocation) {
        mUserLocation = userLocation;
    }

    public String getLocationImageUrl(){
        return String.format("http://maps.google.com/maps/api/staticmap?center=%f,%f&zoom=16&size=6800x280&sensor=false",
                mUserLocation.getLatitude(), mUserLocation.getLongitude());
    }

    public void onCheckedEnabling(final boolean checked){
        Realm db = Realm.getDefaultInstance();
        db.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                mUserLocation.setEnabled(checked);
                realm.copyToRealmOrUpdate(mUserLocation);
            }
        });
    }

    public UserLocation getUserLocation() {
        return mUserLocation;
    }
}
