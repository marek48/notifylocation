package com.notifylocation.ui.components.locationDetails;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.SupportMapFragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.notifylocation.R;
import com.notifylocation.databinding.ActivityLocationDetailsBinding;
import com.notifylocation.models.UserLocation;
import com.notifylocation.service.LocationCheckingService;
import com.notifylocation.utils.PlatformUtils;

public class LocationDetailsActivity extends AppCompatActivity{
    private static final String KEY_USER_LOCATION = "key_user_location";
    private static final int PERMISSION_LOCATION = 1;
    private ActivityLocationDetailsBinding mBinding;
    private LocationDetailsViewModel mModel;

    private GoogleApiClient mGoogleApiClient;

    public static Intent getActivityIntent(Context context, UserLocation userLocation){
        Intent intent = new Intent(context, LocationDetailsActivity.class);
        if(userLocation != null){
            intent.putExtra(KEY_USER_LOCATION, userLocation);
        }
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_location_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setModel();
    }

    private void setModel() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        UserLocation userLocation = null;
        userLocation = getIntent().getParcelableExtra(KEY_USER_LOCATION);
        mModel = new LocationDetailsViewModel(this, userLocation);

        setupLocationService();

        mapFragment.getMapAsync(mModel);
        mBinding.setModel(mModel);
    }

    private void setupLocationService() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) &&
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                Snackbar.make(mBinding.coordinator, R.string.blocked, Snackbar.LENGTH_LONG);

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_LOCATION);
            }
        } else {
            permissionGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSION_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionGranted();
            } else {
                showSnackbar(R.string.blocked);
            }
        }
    }

    private void permissionGranted(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(mModel)
                .addApi(LocationServices.API)
                .build();
        mModel.setGoogleApiClient(mGoogleApiClient);
        if(!PlatformUtils.isServiceRunning(this, LocationCheckingService.class)){
            startService(new Intent(this, LocationCheckingService.class));
        }
    }

    public void showSnackbar(int message){
        Snackbar.make(mBinding.coordinator, getString(message), Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected()){
            mGoogleApiClient.disconnect();
        }
    }


}
