package com.notifylocation.ui.components.list;

import com.notifylocation.databinding.ItemLocationBinding;
import com.notifylocation.ui.components.locationDetails.LocationDetailsActivity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Maryan Melnychuk on 9/11/16.
 */

public class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.LocationViewHolder> {
    private List<LocationItemViewModel> mLocations;

    public void setLocations(List<LocationItemViewModel> locations) {
        mLocations = locations;
        notifyDataSetChanged();
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemLocationBinding binding = ItemLocationBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new LocationViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        if(holder.binding != null){
            holder.binding.setModel(mLocations.get(position));
            holder.binding.executePendingBindings();
        }
    }

    @Override
    public int getItemCount() {
        if(mLocations != null){
            return mLocations.size();
        }
        return 0;
    }

    public static class LocationViewHolder extends RecyclerView.ViewHolder{
        ItemLocationBinding binding;

        public LocationViewHolder(ItemLocationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(LocationViewHolder.this.binding.getModel() != null){
                        v.getContext().startActivity(LocationDetailsActivity
                                .getActivityIntent(v.getContext(), LocationViewHolder.this.binding.getModel().getUserLocation()));
                    }
                }
            });
        }
    }
}
