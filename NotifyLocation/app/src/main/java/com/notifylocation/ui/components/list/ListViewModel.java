package com.notifylocation.ui.components.list;

import com.notifylocation.BR;
import com.notifylocation.models.UserLocation;
import com.notifylocation.service.LocationCheckingService;
import com.notifylocation.ui.components.locationDetails.LocationDetailsActivity;
import com.notifylocation.utils.PlatformUtils;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Maryan Melnychuk on 9/11/16.
 */

public class ListViewModel extends BaseObservable {
    private ListActivity mActivity;
    private RecyclerView.LayoutManager mLayoutManager;
    private LocationsAdapter mAdapter;
    private List<LocationItemViewModel> mLocations;

    @BindingAdapter("bind:locations")
    public static void setLocations(RecyclerView view, List<LocationItemViewModel> locations){
        if(locations != null && locations.size() > 0){
            LocationsAdapter adapter = (LocationsAdapter) view.getAdapter();
            adapter.setLocations(locations);
        }
    }

    public ListViewModel(ListActivity activity) {
        mActivity = activity;
        mLayoutManager = new LinearLayoutManager(activity);
        mAdapter = new LocationsAdapter();
        loadingLocations();
    }

    private void loadingLocations() {
        Realm db = Realm.getDefaultInstance();
        RealmResults<UserLocation> results = db.where(UserLocation.class).findAll();
        if(results.size() > 0){
            mLocations = new ArrayList<>();
            for(UserLocation userLocation : results){
                mLocations.add(new LocationItemViewModel(userLocation));
            }
            notifyPropertyChanged(BR.locations);

            if(!PlatformUtils.isServiceRunning(mActivity, LocationCheckingService.class)) {
                mActivity.startService(new Intent(mActivity, LocationCheckingService.class));
            }
        } else {
            mLocations = null;
            notifyPropertyChanged(BR.locations);
        }
    }

    public void onAddLocation(View view){
        mActivity.startActivity(LocationDetailsActivity.getActivityIntent(mActivity, null));
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return mLayoutManager;
    }

    public LocationsAdapter getAdapter() {
        return mAdapter;
    }

    @Bindable
    public List<LocationItemViewModel> getLocations() {
        return mLocations;
    }

    public void refresh() {
        loadingLocations();
    }
}
