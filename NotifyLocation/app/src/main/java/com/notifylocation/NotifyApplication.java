package com.notifylocation;

import com.crashlytics.android.Crashlytics;
import com.notifylocation.service.LocationCheckingService;
import com.notifylocation.utils.PlatformUtils;

import android.app.Application;
import android.content.Intent;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

/**
 * Created by Maryan Melnychuk on 9/11/16.
 */

public class NotifyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Timber.d("Application onCreate");
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder(this)
                .name("notifyLocation.realm")
                .build());
    }
}
