package com.notifylocation.utils;

import com.bumptech.glide.Glide;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

/**
 * Created by Maryan Melnychuk on 9/11/16.
 */

public final class BindingUtils {

    @BindingAdapter("bind:imageUrl")
    public static void setImageUrl(ImageView imageView, String url){
        Glide.with(imageView.getContext())
                .load(url)
                .centerCrop()
                .into(imageView);
    }
}
