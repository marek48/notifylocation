package com.notifylocation.utils;

import android.app.ActivityManager;
import android.content.Context;

/**
 * Created by Maryan Melnychuk on 9/26/16.
 */

public class PlatformUtils {

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
