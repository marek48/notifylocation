package com.notifylocation.service;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.notifylocation.BR;
import com.notifylocation.R;
import com.notifylocation.events.OnUserLocationsChanged;
import com.notifylocation.models.UserLocation;
import com.notifylocation.ui.components.list.LocationItemViewModel;
import com.notifylocation.utils.LocationUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by Maryan Melnychuk on 9/26/16.
 */

public class LocationCheckingService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private List<UserLocation> mLocations;

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("Location checking service onCreate");
        EventBus.getDefault().register(this);
        loadLocations();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(7000);
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timber.d("onStartCommand");
        mGoogleApiClient.connect();
        return super.onStartCommand(intent, flags, startId);
    }

    private void loadLocations() {
        Realm db = Realm.getDefaultInstance();
        RealmResults<UserLocation> results = db.where(UserLocation.class).findAll();
        if(results.size() > 0){
            mLocations = new ArrayList<>();
            for(UserLocation userLocation : results){
                mLocations.add(userLocation);
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Timber.d("Connected to location service");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Timber.w("Service doesn't have location permissions");
            stopSelf();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        Timber.d("Location changed long: %s, lat: %s", String.valueOf(location.getLongitude()), String.valueOf(location.getLatitude()));
        if(mLocations != null) {
            checkUserLocations(location);
        }
    }

    private void checkUserLocations(Location location) {
        for (UserLocation userLocation : mLocations){
            int distFromPlace = (int) LocationUtils.distFrom(userLocation.getLatitude(), userLocation.getLongitude(),
                    location.getLatitude(), location.getLongitude());
            if(distFromPlace < userLocation.getRadius()){
                Timber.d("User near '%s', dist: %dm", userLocation.getName(), distFromPlace);
                showNotification(userLocation, distFromPlace);
            } else {
                Timber.d("User not near '%s', dist: %dm", userLocation.getName(), distFromPlace);
            }
        }
    }

    private void showNotification(UserLocation userLocation, int dist) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.icon)
                        .setContentTitle(String.format("You near %s place", userLocation.getName()))
                        .setContentText(String.format("You just %dm far from %s.", dist, userLocation.getName()));
        Uri gmmIntentUri = Uri.parse(String.format("geo:%f,%f", userLocation.getLatitude(), userLocation.getLongitude()));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        userLocation.getName().hashCode(),
                        mapIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);
        int mNotificationId = 001;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    @Subscribe
    public void onEvent(OnUserLocationsChanged event){
        loadLocations();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Timber.e("onConnectionFailed");
        if(mGoogleApiClient != null){
            mGoogleApiClient.reconnect();
        }
    }

    @Override
    public void onDestroy() {
        Timber.d("onDestroy");
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        mGoogleApiClient.disconnect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
