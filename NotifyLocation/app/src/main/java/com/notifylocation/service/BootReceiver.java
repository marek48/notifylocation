package com.notifylocation.service;

import com.notifylocation.models.UserLocation;
import com.notifylocation.utils.PlatformUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Maryan Melnychuk on 9/28/16.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Realm db = Realm.getDefaultInstance();
        RealmResults<UserLocation> results = db.where(UserLocation.class).findAll();
        if(results.size() > 0){
            if(!PlatformUtils.isServiceRunning(context, LocationCheckingService.class)) {
                context.startService(new Intent(context, LocationCheckingService.class));
            }
        }
    }
}
